# silver-screen

This is my first project called "Silver Screen". It is a simple website about different movies and actors. Frameworks used:

Node.js
React.js
Hosted on Heroku. That is probably all :)

If there is one thing that I've come to learn with SaaS, is that the learning never stops. The actions you take along the way can shape the future of your product and determine the performance of your company.

Whether you are burning the midnight oil building your startup or are already in the market testing the waters, these 5 great articles will help you wade through the challenges of [SaaS product](https://www.eleken.co/) management.

1.  [SaaS Trends of 2021 Every Business Owner Should Know](https://www.eleken.co/blog-posts/saas-trends)

The Software-as-a-Service industry is one of the lucky ones who not only survived the pandemic but managed to grow and attract new clients and investments. If 2020 has taught us anything, it is that forecasts are very unsure. Well, not only forecasts...

2.  [14 Essential UX Research Methods](https://www.eleken.co/blog-posts/14-essential-ux-research-methods)

Many people wonder, what do UX researchers do apart from talking to users about their experience? Can experience be measured?

UX research is a huge part of UI/UX design services. Here is the list of our favorite UX research methods we use regularly to answer those questions.

3.  [Product Designer Job Description](https://www.eleken.co/blog-posts/product-designer-job-description-find-a-perfect-candidate-for-your-team)

Very often we think of a designer as a "beautician" for websites, applications, and other digital products. In fact, creating an appealing user interface is only one point from a product designer job description.

4.  [Design Thinking Examples: Five Real Stories](https://www.eleken.co/blog-posts/design-thinking-examples-five-real-stories)

No other area of design requires such deep immersion in the client's world as UI/UX design. To create a user-friendly and practical product, it is necessary to understand the customers' pains, needs, and expectations. This is what design thinking is all about.

5.  [SaaS Onboarding: Educate, Engage and Retain Your Customers](https://www.eleken.co/blog-posts/saas-onboarding-educate-engage-and-retain-your-customers)

As a product owner, you may know the value proposition of your SaaS perfectly well. But there is no use in cutting-edge functionality and tailored user interface if your customers have trouble figuring out how they can benefit from using your product. And that's where SaaS onboarding comes in handy.